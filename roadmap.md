## Roadmap 

The Roadmap of the Smart Data Models Initiative is the following: 

* Integration of other well documented, adopted and open standards. Suggest [here](https://smartdatamodels.org/index.php/submit-an-issue-2/)

* Integration with other tools like data spaces, open data portals, any tool for data sharing. Suggest [here](https://smartdatamodels.org/index.php/submit-an-issue-2/)

* Creation of the [pysmartdatamodels](https://github.com/smart-data-models/data-models/tree/master/pysmartdatamodels) with all the attributes, data models and functions with extra informatio from them

* There is an option in smartdatamodels.org for listing other [initiatives pending](https://smartdatamodels.org/index.php/smart-data-models-wish-list/)

* Automation of the testing tools to check consistency within the data models







